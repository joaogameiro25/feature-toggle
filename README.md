# feature-toggle

Feature Toggle Web App

## Getting started

 - Git clone this project to a local folder in your filesystem.
 - Check if you have Java 17+ installed. 


## Build JAR
- cd into backend: `cd feature-toggle/backend`
- Run: `./gradlew build`


## Run in Dev mode
 - Make sure an instance of postgres is running with the same details that can be found in application.properties
 - cd into backend: `cd feature-toggle/backend`
 - Run: `./gradlew bootRun`
 - cd into frontend: `cd feature-toggle/frontend`
 - Run: `ng serve`
 - Open your browser on http://localhost:4200/

## Run with Docker
 - In order to run it with docker, make sure you have it installed first.
 - From the project root, simply run: `docker-compose up`
 - Open your browser on http://localhost:4200/

## Run Tests
 - cd into backend: `cd feature-toggle/backend`
 - Run: `./gradlew test`
 - cd into backend: `cd feature-toggle/frontend`
 - Run: `ng test`

## API
 - The API will be available at http://localhost:8080/. In order to get the Feature Toggle status list of a given customer, simply request:
```
POST http://localhost:8080/api/v1/features/status

Body:
{
    "customerId": "<CUSTOMER_ID>"
}
```


