package com.example.backend;

import com.example.backend.domain.FeatureToggle;
import com.example.backend.repository.FeatureToggleRepository;
import com.example.backend.service.FeatureToggleService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.AssertionErrors.*;


@SpringBootTest
@ActiveProfiles(profiles = "test")
@ExtendWith(SpringExtension.class)
public class FeatureToggleServiceTests {

    @InjectMocks
    private FeatureToggleService featureToggleService;

    @Mock
    private FeatureToggleRepository featureToggleRepository;

    @Test
    public void testGetSpecificFeatureToggle() {
        long ftId = 1;
        FeatureToggle mockFeatureToggle = new FeatureToggle();
        mockFeatureToggle.setId(ftId);
        mockFeatureToggle.setInverted(true);
        mockFeatureToggle.setTechnicalName("ft-1");
        mockFeatureToggle.setCustomerIds(List.of("1"));

        when(featureToggleRepository.findById(ftId)).thenReturn(Optional.of(mockFeatureToggle));

        Optional<FeatureToggle> result = featureToggleService.getSpecificFeatureToggle(ftId);
        assertNotNull("Result should not be null.", result);
        assertTrue("Result should contain data.", result.isPresent());

        FeatureToggle ft = result.get();
        assertEquals("Ids should match.", ftId, ft.id);
        assertEquals("Technical names should match.", "ft-1", ft.getTechnicalName());
        assertEquals("Customer IDs should match.", "1", result.get().getCustomerIds().get(0));
    }

    @Test
    public void testAllFeatureToggles() {
        long ft1 = 1;
        long ft2 = 2;

        FeatureToggle featureToggle1 = new FeatureToggle();
        featureToggle1.setId(ft1);
        featureToggle1.setInverted(true);
        featureToggle1.setTechnicalName("ft-1");
        featureToggle1.setCustomerIds(List.of("1"));

        FeatureToggle featureToggle2 = new FeatureToggle();
        featureToggle2.setId(ft2);
        featureToggle2.setInverted(false);
        featureToggle2.setTechnicalName("ft-2");
        featureToggle2.setCustomerIds(List.of("2"));

        List<FeatureToggle> mockFeatureToggles = new ArrayList<>(List.of(featureToggle1, featureToggle2));

        when(featureToggleRepository.findAll()).thenReturn(mockFeatureToggles);

        List<FeatureToggle> result = featureToggleService.getAllFeatureToggles();

        assertNotNull("Result should not be null.", result);
        assertFalse("Result should contain data.", result.isEmpty());
        assertEquals("Expected 2 feature toggles", 2, result.size());

        FeatureToggle firstFeatureToggle = result.get(0);
        assertEquals("First feature toggle ID should be 1", ft1, firstFeatureToggle.getId());
        assertEquals("First feature toggle should be inverted", true, firstFeatureToggle.isInverted());
        assertEquals("First feature toggle technical name should be ft-1", "ft-1", firstFeatureToggle.getTechnicalName());
        assertEquals("First feature toggle customer ID should be 1", "1", firstFeatureToggle.getCustomerIds().get(0));

        FeatureToggle secondFeatureToggle = result.get(1);
        assertEquals("Second feature toggle ID should be 2", ft2, secondFeatureToggle.getId());
        assertEquals("Second feature toggle should not be inverted", false, secondFeatureToggle.isInverted());
        assertEquals("Second feature toggle technical name should be ft-2", "ft-2", secondFeatureToggle.getTechnicalName());
        assertEquals("Second feature toggle customer ID should be 2", "2", secondFeatureToggle.getCustomerIds().get(0));
    }

}
