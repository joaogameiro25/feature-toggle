package com.example.backend.controller;

import com.example.backend.domain.FeatureToggle;
import com.example.backend.dto.*;
import com.example.backend.service.FeatureToggleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api/v1/features")
public class FeatureToggleController {
    // Test

    private final FeatureToggleService featureToggleService;

    @Autowired
    private FeatureToggleController(FeatureToggleService featureToggleService) {
        this.featureToggleService = featureToggleService;
    }

    /**
     * Retrieves all feature toggles.
     *
     * @return A list of all feature toggles or an empty response if none exist.
     */
    @GetMapping
    private ResponseEntity<List<FeatureToggle>> getAllFeatureToggles() {
        List<FeatureToggle> featureToggles = featureToggleService.getAllFeatureToggles();
        return featureToggles.isEmpty() ? ResponseEntity.ok(Collections.emptyList()) : ResponseEntity.ok(featureToggles);
    }

    /**
     * Retrieves a specific feature toggle.
     *
     * @param id ID of the feature toggle to retrieve.
     * @return The requested feature toggle, a bad request response if the ID is null or a not found.
     */
    @GetMapping("{id}")
    private ResponseEntity<FeatureToggle> getSpecificFeatureToggle(@PathVariable Long id) {
        if (id == null) {
            return ResponseEntity.badRequest().build();
        }

        Optional<FeatureToggle> featureToggle = featureToggleService.getSpecificFeatureToggle(id);
        return featureToggle.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Creates a new feature toggle.
     *
     * @param featureToggle The feature toggle to create.
     * @return The created feature toggle or a bad request response if creation fails.
     */
    @PostMapping
    private ResponseEntity<FeatureToggle> createFeatureToggle(@RequestBody FeatureToggle featureToggle) {
        try {
            FeatureToggle ft = this.featureToggleService.save(featureToggle);
            return ResponseEntity.ok(ft);
        } catch (IllegalArgumentException | OptimisticLockingFailureException ex) {
            return ResponseEntity.badRequest().build();
        }
    }

    /**
     * Retrieves the status of customer specific feature toggles.
     *
     * @param featureRequest The request containing the customer ID.
     * @return The status of the customer's feature toggles, a bad request response if the customer ID is null
     * or an empty response in case there's no feature toggles associated to that customer.
     */
    @PostMapping("status")
    public ResponseEntity<FeatureResponse> getCustomerFeatureStatus(@RequestBody FeatureRequest featureRequest) {
        Long customerId = featureRequest.getCustomerId();
        if (customerId == null) {
            return ResponseEntity.badRequest().build();
        }

        List<FeatureStatus> featureStatusList = this.featureToggleService.getCustomerFeatureTogglesStatus(customerId);
        if (featureStatusList.isEmpty()) {
            return ResponseEntity.ok(new FeatureResponse(Collections.emptyList()));
        }

        FeatureResponse featureResponse = new FeatureResponse(featureStatusList);
        return ResponseEntity.ok(featureResponse);
    }

    /**
     * Updates a specific feature toggle.
     *
     * @param id ID of the feature toggle to update.
     * @param updatedFeatureToggle The updated feature toggle.
     * @return The updated feature toggle, not found if the feature toggle doesn't exist or
     * a bad request response if the ID is null or the update fails.
     */
    @PatchMapping("{id}")
    private ResponseEntity<FeatureToggle> updateFeatureToggle(@PathVariable Long id, @RequestBody FeatureToggle updatedFeatureToggle) {
        if (id == null) {
            return ResponseEntity.badRequest().build();
        }

        Optional<FeatureToggle> existingFeatureToggleOpt = this.featureToggleService.getSpecificFeatureToggle(id);
        if (existingFeatureToggleOpt.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        FeatureToggle ft = this.featureToggleService.updateFeatureToggle(existingFeatureToggleOpt.get(), updatedFeatureToggle);
        try {
            return ResponseEntity.ok(this.featureToggleService.save(ft));
        } catch (IllegalArgumentException | OptimisticLockingFailureException ex) {
            return ResponseEntity.badRequest().build();
        }
    }

    /**
     * Deletes a specific feature toggle.
     *
     * @param id ID of the feature toggle to delete.
     * @return An empty response indicating deletion was successful,
     * a bad request response if the ID is null or not found if feature toggle doesn't exist.
     */
    @DeleteMapping("{id}")
    private ResponseEntity<FeatureToggle> deleteFeatureToggle(@PathVariable Long id) {
        if (id == null) {
            return ResponseEntity.badRequest().build();
        }

        Optional<FeatureToggle> existingFeatureToggleOpt = featureToggleService.getSpecificFeatureToggle(id);
        if (existingFeatureToggleOpt.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        featureToggleService.deleteSpecificFeatureToggle(id);
        return ResponseEntity.noContent().build();
    }
}
