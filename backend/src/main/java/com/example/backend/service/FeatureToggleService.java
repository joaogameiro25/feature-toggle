package com.example.backend.service;

import com.example.backend.domain.FeatureToggle;
import com.example.backend.dto.FeatureStatus;
import com.example.backend.repository.FeatureToggleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FeatureToggleService {

    private final FeatureToggleRepository featureToggleRepository;

    @Autowired
    private FeatureToggleService(FeatureToggleRepository featureToggleRepository) {
        this.featureToggleRepository = featureToggleRepository;
    }

    public List<FeatureToggle> getAllFeatureToggles() {
        return this.featureToggleRepository.findAll();
    }

    public Optional<FeatureToggle> getSpecificFeatureToggle(Long id) {
        return this.featureToggleRepository.findById(id);
    }

    public FeatureToggle save(FeatureToggle featureToggle) {
        return this.featureToggleRepository.save(featureToggle);
    }


    public List<FeatureToggle> getCustomerFeatureToggles(Long customerId) {
        return this.featureToggleRepository.findByCustomerId(customerId);
    }

    public List<FeatureStatus> getCustomerFeatureTogglesStatus(Long customerId) {
        List<FeatureToggle> featureToggleList = getCustomerFeatureToggles(customerId);
        if (featureToggleList.isEmpty()) {
            return Collections.emptyList();
        }

        return featureToggleList
                .stream()
                .map(ft ->
                        new FeatureStatus(
                                ft.getTechnicalName(),
                                true, // assuming that a feature toggle needs to be active to be assigned to a customer
                                ft.isInverted(),
                                ft.getExpiresOn() != null && ft.getExpiresOn().isBefore(LocalDateTime.now())
                        )
                ).collect(Collectors.toList());
    }

    public void deleteSpecificFeatureToggle(Long id) {
        this.featureToggleRepository.deleteById(id);
    }

    public FeatureToggle updateFeatureToggle(FeatureToggle featureToggle, FeatureToggle updatedFeatureToggle) {
        featureToggle.setDisplayName(updatedFeatureToggle.getDisplayName());
        featureToggle.setTechnicalName(updatedFeatureToggle.getTechnicalName());
        featureToggle.setDescription(updatedFeatureToggle.getDescription());
        featureToggle.setExpiresOn(updatedFeatureToggle.getExpiresOn());
        featureToggle.setInverted(updatedFeatureToggle.isInverted());
        featureToggle.setCustomerIds(updatedFeatureToggle.getCustomerIds());

        return featureToggle;
    }
}
