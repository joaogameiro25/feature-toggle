package com.example.backend.repository;

import com.example.backend.domain.FeatureToggle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;


@Repository
public interface FeatureToggleRepository extends JpaRepository<FeatureToggle, Long> {
    @Query("select ft from FeatureToggle ft where :customerId member of ft.customerIds")
    List<FeatureToggle> findByCustomerId(Long customerId);

}
