package com.example.backend.dto;

public class FeatureStatus {
    @lombok.Setter
    @lombok.Getter
    private String name;
    @lombok.Setter
    @lombok.Getter
    private boolean active;
    @lombok.Setter
    @lombok.Getter
    private boolean inverted;
    @lombok.Setter
    @lombok.Getter
    private boolean expired;

    public FeatureStatus(String name, boolean active, boolean inverted, boolean expired) {
        this.name = name;
        this.active = active;
        this.inverted = inverted;
        this.expired = expired;
    }
}
