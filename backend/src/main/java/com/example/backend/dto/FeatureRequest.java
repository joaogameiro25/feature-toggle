package com.example.backend.dto;

import com.fasterxml.jackson.annotation.JsonCreator;

public class FeatureRequest {
    @lombok.Setter
    @lombok.Getter
    private Long customerId;

    @JsonCreator
    private FeatureRequest(Long customerId) {
        this.customerId = customerId;
    }

}

