package com.example.backend.dto;

import java.util.List;

public class FeatureResponse {
    @lombok.Getter
    @lombok.Setter
    private List<FeatureStatus> features;

    public FeatureResponse(List<FeatureStatus> features) {
        this.features = features;
    }

}
