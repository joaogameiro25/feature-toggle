package com.example.backend.domain;

import jakarta.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "feature_toggle")
public class FeatureToggle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @lombok.Getter
    @lombok.Setter
    public Long id;
    @lombok.Getter
    @lombok.Setter
    private String displayName;
    @lombok.Getter
    @lombok.Setter
    @Column(nullable = false)
    private String technicalName;
    @lombok.Getter
    @lombok.Setter
    private LocalDateTime expiresOn;
    @lombok.Getter
    @lombok.Setter
    private String description;
    @lombok.Getter
    @lombok.Setter
    @Column(nullable = false)
    private boolean inverted;
    @lombok.Getter
    @lombok.Setter
    @ElementCollection(targetClass = String.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "customer_ids", joinColumns = @JoinColumn(name = "feature_toggle_id"))
    @Column(name = "customer_id", nullable = false)
    private List<String> customerIds;
}
