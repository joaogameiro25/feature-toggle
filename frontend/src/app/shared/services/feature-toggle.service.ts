import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FeatureToggle } from 'src/app/shared/domain/FeatureToggle';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FeatureToggleService {

  private static readonly FEATURE_TOGGLE_API_URL = '/api/v1';

  constructor(private readonly httpClient: HttpClient) { }

  getAllFeatureToggles(): Observable<FeatureToggle[]> {
    return this.httpClient.get<any>(`${FeatureToggleService.FEATURE_TOGGLE_API_URL}/features`)
  }

  getSpecificFeatureToggle(featureToggleId: number): Observable<FeatureToggle> {
    return this.httpClient.get<any>(`${FeatureToggleService.FEATURE_TOGGLE_API_URL}/features/${featureToggleId}`)
  }

  createFeatureToggle(featureToggle: FeatureToggle): Observable<FeatureToggle> {
    return this.httpClient.post<FeatureToggle>(`${FeatureToggleService.FEATURE_TOGGLE_API_URL}/features`, featureToggle);
  }

  updateFeatureToggle(featureToggleId: number, updatedfeatureToggle: FeatureToggle) {
    const url = `${FeatureToggleService.FEATURE_TOGGLE_API_URL}/features/${featureToggleId}`;
    return this.httpClient.patch<FeatureToggle>(url, updatedfeatureToggle);
  }

  deleteFeatureToggle(featureToggleId: number) {
    const url = `${FeatureToggleService.FEATURE_TOGGLE_API_URL}/features/${featureToggleId}`;
    return this.httpClient.delete<FeatureToggle>(url);
  }
}
