export class FeatureToggle {
  id?: number;
  displayName: string;
  technicalName: string;
  expiresOn: string;
  description: string;
  inverted: boolean;
  customerIds: string;

  constructor(dto: any) {
    this.id = dto.id
    this.displayName = dto.displayName;
    this.technicalName = dto.technicalName;
    this.expiresOn = dto.expiresOn;
    this.description = dto.description;
    this.inverted = dto.inverted;
    this.customerIds = dto.customerIds ?? dto.customerIds.toString();
  }
}
