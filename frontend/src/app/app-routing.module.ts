import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FeatureToggleListComponent } from './feature-toggle/components/feature-toggle-list/feature-toggle-list.component';
import { FeatureToggleDetailsComponent } from './feature-toggle/components/feature-toggle-details/feature-toggle-details.component';
import { FeatureToggleFormComponent } from './feature-toggle/components/feature-toggle-form/feature-toggle-form.component';


const routes: Routes = [
  { path: '', redirectTo: 'features', pathMatch: 'full' },
  { path: 'features', component: FeatureToggleListComponent },
  { path: 'features/new', component: FeatureToggleFormComponent },
  { path: 'features/:id', component: FeatureToggleDetailsComponent },
  { path: 'features/:id/edit', component: FeatureToggleFormComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
