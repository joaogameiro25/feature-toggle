import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FeatureToggleListComponent } from './feature-toggle/components/feature-toggle-list/feature-toggle-list.component';
import { FeatureToggleDetailsComponent } from './feature-toggle/components/feature-toggle-details/feature-toggle-details.component';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FeatureToggleFormComponent } from './feature-toggle/components/feature-toggle-form/feature-toggle-form.component';
import { LoadingSpinnerComponent } from './shared/loading-spinner/loading-spinner.component';
import { FormsModule } from '@angular/forms';
import { ErrorPanelComponent } from './shared/error-panel/error-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FeatureToggleListComponent,
    FeatureToggleDetailsComponent,
    FeatureToggleFormComponent,
    LoadingSpinnerComponent,
    ErrorPanelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
