import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureToggleFormComponent } from './feature-toggle-form.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';

describe('FeatureToggleFormComponent', () => {
  let component: FeatureToggleFormComponent;
  let fixture: ComponentFixture<FeatureToggleFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FeatureToggleFormComponent],
      imports: [RouterTestingModule, HttpClientTestingModule, FormsModule]
    });
    fixture = TestBed.createComponent(FeatureToggleFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
