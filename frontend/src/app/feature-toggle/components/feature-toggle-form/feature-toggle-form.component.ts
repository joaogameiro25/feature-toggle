import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, of } from 'rxjs';
import { FeatureToggleService } from 'src/app/shared/services/feature-toggle.service';

@Component({
  selector: 'app-feature-toggle-form',
  templateUrl: './feature-toggle-form.component.html',
  styleUrls: ['./feature-toggle-form.component.scss']
})
export class FeatureToggleFormComponent {

  errorMessage: string | null = null;
  featureToggleId = this.route.snapshot.params['id']
  queryParams = this.route.snapshot.queryParams

  formProperties = {
    displayName: this.queryParams['displayName'] || '',
    technicalName: this.queryParams['technicalName'] || '',
    expiresOn: this.queryParams['expiresOn'] || '',
    description: this.queryParams['description'] || '',
    inverted: this.queryParams['inverted'] === 'true',
    customerIds: this.queryParams['customerIds'] ? this.queryParams['customerIds'].toString() : ''
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private featureToggleService: FeatureToggleService
  ) { }

  onSubmit() {
    this.formProperties.customerIds = this.formProperties.customerIds.split(',')
    if(this.featureToggleId != null) {
      this.featureToggleService.updateFeatureToggle(this.featureToggleId, this.formProperties).pipe(
        catchError(err => {
          this.errorMessage = `${err.status}:${err.statusText}`
          return of(err)
        }),
      ).subscribe(res => this.router.navigate(['..'], { relativeTo: this.route }))
    } else {
      this.featureToggleService.createFeatureToggle(this.formProperties).pipe(
        catchError(err => {
          this.errorMessage = `${err.status}:${err.statusText}`
          return of(err)
        }),
      ).subscribe(res => this.router.navigate(['..'], { relativeTo: this.route }))
    }
  }

  onCancel() {
    this.router.navigate(['..'], { relativeTo: this.route} )
  }

}
