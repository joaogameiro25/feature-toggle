import { Component } from '@angular/core';
import { FeatureToggleService } from '../../../shared/services/feature-toggle.service';
import { FeatureToggle } from 'src/app/shared/domain/FeatureToggle';
import { catchError, Observable, of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-feature-toggle-list',
  templateUrl: './feature-toggle-list.component.html',
  styleUrls: ['./feature-toggle-list.component.scss']
})
export class FeatureToggleListComponent {

  currentPage: number = 1
  listSize: number = 10

  toggles$: Observable<FeatureToggle[]> = this.featureToggleService.getAllFeatureToggles().pipe(
    catchError((err) => {
      return of([])
    })
  )

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private featureToggleService: FeatureToggleService,
  ) { }

  get startIndex(): number {
    return (this.currentPage - 1) * this.listSize
  }
  get endIndex(): number {
    return this.startIndex + this.listSize;
  }

  onClickToggle(toggleId: number | undefined) {
    console.assert(toggleId != undefined);
    this.router.navigate([toggleId], { relativeTo: this.route });
  }

  onChangeCurrentPage(currentPage: number) {
    this.currentPage = currentPage;
  }

}
