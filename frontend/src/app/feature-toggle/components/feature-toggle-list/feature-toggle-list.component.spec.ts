import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureToggleListComponent } from './feature-toggle-list.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing'; // Import HttpClientTestingModule
import { LoadingSpinnerComponent } from 'src/app/shared/loading-spinner/loading-spinner.component';

describe('FeatureToggleListComponent', () => {
  let component: FeatureToggleListComponent;
  let fixture: ComponentFixture<FeatureToggleListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FeatureToggleListComponent, LoadingSpinnerComponent],
      imports: [RouterTestingModule, HttpClientTestingModule]
    });
    fixture = TestBed.createComponent(FeatureToggleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
