import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureToggleDetailsComponent } from './feature-toggle-details.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { LoadingSpinnerComponent } from 'src/app/shared/loading-spinner/loading-spinner.component';

describe('FeatureToggleDetailsComponent', () => {
  let component: FeatureToggleDetailsComponent;
  let fixture: ComponentFixture<FeatureToggleDetailsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FeatureToggleDetailsComponent, LoadingSpinnerComponent],
      imports: [RouterTestingModule, HttpClientTestingModule]
    });
    fixture = TestBed.createComponent(FeatureToggleDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
