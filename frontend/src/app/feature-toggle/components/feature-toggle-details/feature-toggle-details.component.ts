import { FeatureToggleService } from '../../../shared/services/feature-toggle.service';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, Observable, of, switchMap } from 'rxjs';
import { FeatureToggle } from 'src/app/shared/domain/FeatureToggle';

@Component({
  selector: 'app-feature-toggle-details',
  templateUrl: './feature-toggle-details.component.html',
  styleUrls: ['./feature-toggle-details.component.scss']
})
export class FeatureToggleDetailsComponent {

  errorMessage: string | null = null;

  featureId = this.route.snapshot.params['id']
  featureToggleDetails$: Observable<FeatureToggle> =
    this.featureToggleService.getSpecificFeatureToggle(this.featureId).pipe(
      catchError((err) => {
        this.errorMessage = `${err.status}:${err.statusText}`
        return of(err)
      })
    )

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private featureToggleService: FeatureToggleService
  ) { }

  onClickEdit(featureToggle: FeatureToggle) {
    this.router.navigate(['edit'], {
      relativeTo: this.route,
      queryParams: {
        displayName: featureToggle.displayName,
        technicalName: featureToggle.technicalName,
        inverted: featureToggle.inverted,
        expiresOn: featureToggle.expiresOn,
        description: featureToggle.description,
        customerIds: featureToggle.customerIds,
      },
    })
  }

  onClickDelete(featureToggleId: number) {
    this.featureToggleService.deleteFeatureToggle(featureToggleId).pipe(
      catchError(err => {
        this.errorMessage = `${err.status}:${err.statusText}`
        return of(err)
      }),
    ).subscribe(res => {
      return this.router.navigate(['..'], { relativeTo: this.route })
    })
  }


}
